# Warung Pokemon - Pokedex

## Live URL

- [Warung Pokedex on surge.sh](https://warung-pokedex.surge.sh/)
- [Warung Pokedex on gitlab-page](https://alter.regalia.gitlab.io/warungpokemons)

_\*these apps are running on shared space, might takes minutes or so before they wake up and accessible_

## Task

- Consume Poke-API GraphQL on https://graphql-pokemon.now.sh
- Create Infinite List of Pokemons
- Create Pokemon details screen
- Filter Pokemons by Types

## Stack

- [React](https://reactjs.org)
- [Apollo GraphQL](https://www.apollographql.com)
- [Material-UI](https://material-ui.com)
- [Parcel](https://parceljs.org)
- [SASS/SCSS](https://sass-lang.com)

## How To Run Locally

1. Clone the repo

```
> git clone https://gitlab.com/Alter.Regalia/warungpokemon.git
```

2. Move into **warungpokemon** directory and install required packages

```
> cd warungpokemon
```

I use [Yarn Package Manager](https://yarnpkg.com) as an NPM alternative

```
> yarn
```

running **npm install** will do just fine

3. Run Application

```
> yarn start
```

or **npm run start**

The browser windows will automatically open at http://localhost:1234

if is not type the url on your browser, also check if the port is being used or not

## Production Build

1. To build distribution package, set _NODE_ENV_ to **production** then simply run **yarn build** command

```
> set NODE_ENV=production
```

```
> yarn build
```

or **npm run build**

2. Distribution package are available in **dist** folder

```
___dist
|___index.html
|___src.*******.js
|___src.*******.js.map
|___src.*******.css
|___src.*******.css.map

```
