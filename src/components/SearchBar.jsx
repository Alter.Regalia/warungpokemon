/* eslint-disable no-param-reassign */
import React from 'react';
import { Container, Chip } from '@material-ui/core';

const types = [
  {
    name: 'Normal',
    active: false,
  },
  {
    name: 'Fire',
    active: false,
  },
  {
    name: 'Water',
    active: false,
  },
  {
    name: 'Grass',
    active: false,
  },
  {
    name: 'Electric',
    active: false,
  },
  {
    name: 'Ice',
    active: false,
  },
  {
    name: 'Fighting',
    active: false,
  },
  {
    name: 'Poison',
    active: false,
  },
  {
    name: 'Ground',
    active: false,
  },
  {
    name: 'Flying',
    active: false,
  },
  {
    name: 'Psychic',
    active: false,
  },
  {
    name: 'Bug',
    active: false,
  },
  {
    name: 'Rock',
    active: false,
  },
  {
    name: 'Ghost',
    active: false,
  },
  {
    name: 'Dragon',
    active: false,
  },
  {
    name: 'Dark',
    active: false,
  },
  {
    name: 'Steel',
    active: false,
  },
  {
    name: 'Fairy',
    active: false,
  },
];

const SearchBar = ({ getFilter }) => {
  const handleChips = (typeName) => {
    types.forEach((type) => {
      if (type.name === typeName) {
        type.active = !type.active;
      }
    });

    const filterList = types
      .filter((type) => type.active === true)
      .map((type) => type.name);

    getFilter(filterList);
  };

  return (
    <>
      <Container className="searchbar-container">
        {types.map((type) => (
          <Chip
            key={type.name}
            label={type.name}
            onClick={() => handleChips(type.name)}
            color={type.active ? 'primary' : 'default'}
          />
        ))}
      </Container>
    </>
  );
};

export default SearchBar;
