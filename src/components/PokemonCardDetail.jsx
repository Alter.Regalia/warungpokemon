import React from 'react';
import {
  Card,
  CardHeader,
  CardContent,
  CardMedia,
  Avatar,
  Paper,
  List,
  ListItem,
  ListItemText,
  Chip,
  Box,
} from '@material-ui/core';
import PokemonEvolutions from './PokemonEvolutions';

const PokemonCardDetail = ({ pokemon }) => (
  <Card className="pokemon-card-detail">
    <CardHeader
      avatar={<Avatar>{pokemon.number}</Avatar>}
      title={pokemon.name}
      subheader={pokemon.classification}
    />
    <CardContent>
      <Box display="flex" flexDirection="row" justifyContent="space-around">
        <Box display="flex" flexDirection="column" justifyContent="center">
          <img src={pokemon.image} alt={pokemon.name} />
          <br />
          {pokemon.evolutions && (
            <PokemonEvolutions evolutions={pokemon.evolutions} />
          )}
        </Box>
        <List>
          <ListItem>
            <ListItemText primary="Max HP" secondary={pokemon.maxHP} />
          </ListItem>
          <ListItem>
            <ListItemText primary="Max CP" secondary={pokemon.maxCP} />
          </ListItem>
        </List>
        <List>
          <ListItem>
            <ListItemText
              primary="Weight"
              secondary={`${pokemon.weight.minimum} - ${pokemon.weight.maximum}`}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Height"
              secondary={`${pokemon.height.minimum} - ${pokemon.height.maximum}`}
            />
          </ListItem>
        </List>
      </Box>
      <br />
      <Box>
        <Paper>
          <List>
            <ListItem>
              <ListItemText primary="Fast Attack(s)" />
            </ListItem>
            {pokemon.attacks.fast.map((fastAttack) => (
              <ListItem key={fastAttack.name}>
                <ListItemText
                  primary={fastAttack.name}
                  secondary={fastAttack.type}
                />
              </ListItem>
            ))}
          </List>
        </Paper>
        <br />
        <Paper>
          <List>
            <ListItem>
              <ListItemText primary="Special Attack(s)" />
            </ListItem>
            {pokemon.attacks.special.map((specialAttack) => (
              <ListItem key={specialAttack.name}>
                <ListItemText
                  primary={specialAttack.name}
                  secondary={specialAttack.type}
                />
              </ListItem>
            ))}
          </List>
        </Paper>
        <br />
        <Paper>
          <List>
            <ListItem>
              <ListItemText primary="Attributes" />
            </ListItem>
            <ListItem>
              <ListItemText primary="Types" />
              <br />
              {pokemon.types.map((type) => (
                <Chip
                  className="chips-margin"
                  key={type}
                  label={type}
                  color="primary"
                />
              ))}
            </ListItem>
            <ListItem>
              <ListItemText primary="Weakness" />
              {pokemon.weaknesses.map((weakness) => (
                <Chip
                  className="chips-margin"
                  key={weakness}
                  label={weakness}
                  color="secondary"
                />
              ))}
            </ListItem>
            <ListItem>
              <ListItemText primary="Resistant" />
              {pokemon.resistant.map((resist) => (
                <Chip className="chips-margin" key={resist} label={resist} />
              ))}
            </ListItem>
          </List>
        </Paper>
      </Box>
    </CardContent>
    <CardMedia image={pokemon.image} title="Live from space album cover" />
  </Card>
);

export default PokemonCardDetail;
