/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import { withRouter } from 'react-router-dom';
import { Chip, Avatar, Box } from '@material-ui/core';

const PokemonEvolutions = ({ evolutions, history }) => {
  const toEvolution = (pokemonId) => {
    history.push(`/p/${pokemonId}`);
  };

  return (
    <Box display="flex" flexDirection="row" justifyContent="space-between">
      {evolutions.map((pokemon) => (
        <a key={pokemon.id} onClick={() => toEvolution(pokemon.id)}>
          <Chip
            label={pokemon.name}
            variant="outlined"
            avatar={<Avatar src={pokemon.image} />}
            clickable
          />
        </a>
      ))}
    </Box>
  );
};

export default withRouter(PokemonEvolutions);
