import React from 'react';
import {
  Card, CardHeader, CardContent, Avatar,
} from '@material-ui/core';

const PokemonCard = ({ pokemon }) => (
  <Card className="pokemon-card">
    <CardHeader
      avatar={<Avatar>{pokemon.number}</Avatar>}
      title={pokemon.name}
      subheader={pokemon.classification}
    />
    <CardContent>
      <img src={pokemon.image} alt={pokemon.name} />
    </CardContent>
  </Card>
);

export default PokemonCard;
