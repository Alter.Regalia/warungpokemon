import React from 'react';

const Error = ({ message }) => (
  <>
    <div className="error">
      <code className="erorr-message">{message}</code>
    </div>
  </>
);

export default Error;
