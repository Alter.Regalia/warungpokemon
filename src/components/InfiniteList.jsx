import React from 'react';
import { Link } from 'react-router-dom';
import { Container } from '@material-ui/core';
import PokemonCard from './PokemonCard';

const InfiniteList = ({ pokemons, onLoadMore }) => {
  const scrollHandler = ({ currentTarget }) => {
    if (
      currentTarget.scrollTop + currentTarget.clientHeight
      >= currentTarget.scrollHeight
    ) {
      onLoadMore();
    }
  };

  if (!pokemons) {
    return null;
  }

  return (
    <Container className="list-container" onScroll={scrollHandler}>
      {pokemons.map((pokemon) => (
        <Link to={`/p/${pokemon.id}`} key={pokemon.id} className="list-item">
          <PokemonCard pokemon={pokemon} />
        </Link>
      ))}
    </Container>
  );
};

export default InfiniteList;
