import ApolloClient from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { InMemoryCache } from 'apollo-cache-inmemory';

const createApolloClient = () => {
  const httpLink = new HttpLink({
    uri: 'https://graphql-pokemon.now.sh',
  });

  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
      graphQLErrors.map(({ message, locations, path }) => console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
      ));
    }

    if (networkError) {
      console.log(`[Network error]: ${networkError}`);
    }
  });

  const cache = new InMemoryCache();

  const link = ApolloLink.from([errorLink, httpLink]);

  const client = new ApolloClient({
    link,
    cache,
  });

  return client;
};

export default createApolloClient;
