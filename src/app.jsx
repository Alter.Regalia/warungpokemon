import React from 'react';
import {
  BrowserRouter as Router, Switch, Route, Link,
} from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';
import { AppBar, Typography, Toolbar } from '@material-ui/core';
import createApolloClient from './ApolloClient';
import PokemonDetail from './screens/PokemonDetail';
import PokemonList from './screens/PokemonList';

const apolloClient = createApolloClient();

const App = () => (
  <Router>
    <ApolloProvider client={apolloClient}>
      <AppBar color="primary" position="sticky">
        <Toolbar>
          <Link to="/">
            <Typography variant="h6" className="app-title">
              Pokedex
            </Typography>
          </Link>
        </Toolbar>
      </AppBar>

      <Switch>
        <Route path="/p/:pokemonId">
          <PokemonDetail />
        </Route>
        <Route path="/">
          <PokemonList />
        </Route>
      </Switch>
    </ApolloProvider>
  </Router>
);

export default App;
