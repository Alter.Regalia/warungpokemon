import React, { useState } from 'react';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import { CircularProgress, Box } from '@material-ui/core';

import Error from '../components/Error';
import InfiniteList from '../components/InfiniteList';
import SearchBar from '../components/SearchBar';

const PokemonGQL = gql(`
  query poke($limit: Int!) {
    pokemons(first: $limit) {
      id
      name
      number
      classification
      types
      image
    }
  }
`);

const PokemonList = () => {
  const [limit, setLimit] = useState(20);
  const [typeFilter, setTypeFilter] = useState([]);

  const { data, error, loading } = useQuery(PokemonGQL, {
    variables: {
      limit,
    },
  });

  const filterPokemon = (pokemons) => {
    const result = [];

    pokemons.filter((pokemon) => pokemon.types.map((type) => {
      if (typeFilter.indexOf(type) !== -1) {
        result.push(pokemon);
      }
      return null;
    }));

    return result;
  };

  return (
    <Box
      display="flex"
      alignContent="center"
      alignItems="center"
      flexDirection="column"
    >
      <div className="search-bar">
        <SearchBar getFilter={(value) => setTypeFilter(value)} />
      </div>
      {data && (
        <InfiniteList
          pokemons={
            typeFilter.length ? filterPokemon(data.pokemons) : data.pokemons
          }
          onLoadMore={() => setLimit(limit + 10)}
        />
      )}

      {loading && <CircularProgress />}

      {error && <Error message={error.message} />}
    </Box>
  );
};

export default PokemonList;
