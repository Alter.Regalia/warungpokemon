import React from 'react';
import gql from 'graphql-tag';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';

import { Container, CircularProgress } from '@material-ui/core';

import Error from '../components/Error';
import PokemonCardDetail from '../components/PokemonCardDetail';

const PokemonGQL = gql(`
  query PokeDetail($id: String) {
    pokemon(id: $id) {
      id
      name
      number
      image
      maxHP
      maxCP
      types
      resistant
      weaknesses
      classification
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      attacks {
        fast {
          name
          type
        }
        special {
          name
          type
        }
      }
      evolutions {
        id
        name
        image
      }
    }
  }
`);

const PokemonDetail = () => {
  const { pokemonId } = useParams();
  const { data, error, loading } = useQuery(PokemonGQL, {
    variables: {
      id: pokemonId,
    },
  });

  return (
    <Container className="pokemon-detail-container">
      {data && !loading && <PokemonCardDetail pokemon={data.pokemon} />}

      {loading && <CircularProgress />}

      {error && <Error message={error.message} />}
    </Container>
  );
};

export default PokemonDetail;
